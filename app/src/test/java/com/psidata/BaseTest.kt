package com.psidata

import com.psidata.box.PsiBoxDataRepository
import com.psidata.box.psiBoxDataRepository
import com.psidata.data.models.PsiModel
import org.hamcrest.core.IsNull
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.check.checkModules
import org.koin.test.inject
import java.io.File

class BaseTest : KoinTest {

    val path: String = "src/test/res/"
//    val responseHandler: ResponseHandler by inject()

    val psiBoxDataRepo: PsiBoxDataRepository by inject()

    @Before
    fun beforeEach() {
        startKoin {
            module {
                psiBoxDataRepository
            }
        }
    }

    @Test
    fun `check Koin definitions`() {
       getKoin().checkModules()
    }

    @After
    fun afterEach() {
        stopKoin()
    }

    @Test
    fun `test mock file path is not empty or null`() {
        val file = File(path + "mockPsiDataSuccess.json")
        Assert.assertNotNull(file)
        Assert.assertTrue("File exist", file.exists())
    }

    @Test
    fun `test mock data is not empty or null`() {
        Assert.assertThat(psiBoxDataRepo.getMockData(), IsNull.notNullValue())
    }

    @Test
    fun `test psi-data mock response`() {
        val psiModel = psiBoxDataRepo.getMockData() as PsiModel
        Assert.assertThat(psiModel, IsNull.notNullValue())
//        val status = responseHandler.handleSuccess(psiModel).status
//        Assert.assertEquals("Equal To Status.SUCCESS", Status.SUCCESS, status)

        psiBoxDataRepo.getPsiMockData(
            cbOnResult = { response ->
                Assert.assertThat(response, IsNull.notNullValue()) // PsiModel data should not empty
                response?.region_metadata?.isNotEmpty()?.let {
                    Assert.assertTrue(
                        "Psi region-data is not empty",
                        it
                    )
                }
            },
            cbOnError = {
            })
    }
}