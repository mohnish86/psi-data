package com.psidata.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.psidata.R
import com.psidata.data.models.MoviesData
import com.psidata.data.models.bindingmodels.MoviesBindingModel
import com.psidata.data.services.networking.Resource
import com.psidata.data.services.networking.Status
import com.psidata.data.viewmodels.MovieDataViewModel
import com.psidata.helper.GenericAppAdapter
import kotlinx.android.synthetic.main.activity_movies.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val movieDataViewModel: MovieDataViewModel by inject()
    private var movieList: MutableList<MoviesBindingModel> = mutableListOf()
    private lateinit var genericAppAdapter: GenericAppAdapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        setupRecyclerview()

        val observer = Observer<Resource<MoviesData>> {
            when (it.status) {
                Status.SUCCESS -> {
                    loader.visibility = View.GONE
                    val movieData = it.data as MoviesData
                    setAdapterData(movieData)
                }
                Status.ERROR -> {
                    loader.visibility = View.GONE
                    // TODO: Perform action here
                }
                Status.LOADING -> loader.visibility = View.VISIBLE
            }
        }
        movieDataViewModel.movieData.observe(this, observer)
    }

    /**
     * Initiate recyclerview
     */
    private fun setupRecyclerview() {
        rv_movies.apply {
            setHasFixedSize(true)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            genericAppAdapter = GenericAppAdapter(movieList)
            rv_movies.adapter = genericAppAdapter
        }
    }

    /**
     * Iterate movies api data to set it in binding model and
     * pass it to generic-binding adapter
     */
    private fun setAdapterData(movieData: MoviesData) {
        movieData.movies.forEach { movie ->
            movieList.add(MoviesBindingModel(movie))
        }
        genericAppAdapter.notifyDataSetChanged()
    }
}
