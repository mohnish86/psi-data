package com.psidata.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.psidata.R
import com.psidata.box.PsiBoxDataRepository
import com.psidata.data.models.RegionMetadata
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.android.ext.android.inject

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

//    private val psiDataViewModel: PsiDataViewModel by viewModel()

    private val psiBoxDataRepository: PsiBoxDataRepository by inject()

    // Store psi region data list
    private lateinit var psiDataList: List<RegionMetadata>

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        /**
         * Psi Data Api call from repository
         *
         * @return cbOnResult  -> when success
         * @return cbOnError -> when gets error
         */
        psiBoxDataRepository.getPsiData(
            cbOnResult = { response ->
                loader.visibility = View.GONE
                lblStatus.text = getString(R.string.psi_status) + response?.api_info?.status.orEmpty()
                psiDataList = response?.region_metadata.orEmpty()
                setPsiDataOnMap() // set psi data onto map
            },
            cbOnError = {
                loader.visibility = View.GONE
            })

//        val observer = Observer<Resource<PsiModel>> {
//            when (it.status) {
//                Status.SUCCESS -> {
//                    loader.visibility = View.GONE
//                    val psiModel = it.data as PsiModel
//                    lblStatus.text = "Status: " + psiModel.api_info.status
//                    psiDataList = psiModel.region_metadata
//                    setPsiDataOnMap()
//                }
//                Status.ERROR -> loader.visibility = View.GONE
//                Status.LOADING -> loader.visibility = View.VISIBLE
//            }
//        }
//        psiDataViewModel.psiData.observe(this, observer)
    }

    /**
     * set PSI data onto map
     */
    private fun setPsiDataOnMap() {
        psiDataList.forEach { regionData ->
            val latitude = regionData.label_location.latitude
            val longitude = regionData.label_location.longitude
            val title = regionData.name
            mMap.addMarker(MarkerOptions().position(LatLng(latitude, longitude)).title(title))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 11f))
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }
}
