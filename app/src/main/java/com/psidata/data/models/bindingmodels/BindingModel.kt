package com.psidata.data.models.bindingmodels

interface BindingModel {

    fun layout(): Int
}