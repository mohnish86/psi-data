package com.psidata.data.models.bindingmodels

import com.psidata.R
import com.psidata.data.models.Movy

class MoviesBindingModel(private val movie: Movy) : BindingModel {

    override fun layout(): Int {
        return R.layout.row_movie_data
    }

    fun movieTitle(): String = movie.title
    fun movieDescription(): String = movie.synopsis
    fun movieTimingAndYear(): String = movie.release_dates.theater
    fun movieImage(): String = movie.posters.thumbnail
}