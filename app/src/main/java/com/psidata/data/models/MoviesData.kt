package com.psidata.data.models

open class MoviesData(
    val link_template: String,
    val links: Links,
    val movies: List<Movy>
): BaseModel()

data class Links(
    val alternate: String,
    val self: String
)

data class Movy(
    val abridged_cast: List<AbridgedCast>,
    val alternate_ids: AlternateIds,
    val critics_consensus: String,
    val id: String,
    val links: LinksX,
    val mpaa_rating: String,
    val posters: Posters,
    val ratings: Ratings,
    val release_dates: ReleaseDates,
    val runtime: Int,
    val synopsis: String,
    val title: String,
    val year: Int
)

data class AbridgedCast(
    val characters: List<String>,
    val id: String,
    val name: String
)

data class AlternateIds(
    val imdb: String
)

data class LinksX(
    val alternate: String,
    val cast: String,
    val reviews: String,
    val self: String,
    val similar: String
)

data class Posters(
    val detailed: String,
    val thumbnail: String
)

data class Ratings(
    val audience_rating: String,
    val audience_score: Int,
    val critics_rating: String,
    val critics_score: Int
)

data class ReleaseDates(
    val theater: String
)