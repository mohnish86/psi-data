package com.psidata.data.models

open class PsiModel(
    val api_info: ApiInfo,
    val items: List<Item>,
    val region_metadata: List<RegionMetadata>
) : BaseModel()

data class ApiInfo(
    val status: String
)

data class Item(
    val readings: Readings,
    val timestamp: String,
    val update_timestamp: String
)

data class Readings(
    val co_eight_hour_max: CoEightHourMax,
    val co_sub_index: CoSubIndex,
    val no2_one_hour_max: No2OneHourMax,
    val o3_eight_hour_max: O3EightHourMax,
    val o3_sub_index: O3SubIndex,
    val pm10_sub_index: Pm10SubIndex,
    val pm10_twenty_four_hourly: Pm10TwentyFourHourly,
    val pm25_sub_index: Pm25SubIndex,
    val pm25_twenty_four_hourly: Pm25TwentyFourHourly,
    val psi_twenty_four_hourly: PsiTwentyFourHourly,
    val so2_sub_index: So2SubIndex,
    val so2_twenty_four_hourly: So2TwentyFourHourly
)

data class CommonForIndexInfo(
    val central: Double,
    val east: Double,
    val national: Double,
    val north: Double,
    val south: Double,
    val west: Double
)

data class CoEightHourMax(val commonForIndexInfo: CommonForIndexInfo)

data class CoSubIndex(val commonForIndexInfo: CommonForIndexInfo)

data class No2OneHourMax(val commonForIndexInfo: CommonForIndexInfo)

data class O3EightHourMax(val commonForIndexInfo: CommonForIndexInfo)

data class O3SubIndex(val commonForIndexInfo: CommonForIndexInfo)

data class Pm10SubIndex(val commonForIndexInfo: CommonForIndexInfo)

data class Pm10TwentyFourHourly(val commonForIndexInfo: CommonForIndexInfo)

data class Pm25SubIndex(val commonForIndexInfo: CommonForIndexInfo)

data class Pm25TwentyFourHourly(val commonForIndexInfo: CommonForIndexInfo)

data class PsiTwentyFourHourly(val commonForIndexInfo: CommonForIndexInfo)

data class So2SubIndex(val commonForIndexInfo: CommonForIndexInfo)

data class So2TwentyFourHourly(val commonForIndexInfo: CommonForIndexInfo)

data class RegionMetadata(
    val label_location: LabelLocation,
    val name: String
)

data class LabelLocation(
    val latitude: Double,
    val longitude: Double
)