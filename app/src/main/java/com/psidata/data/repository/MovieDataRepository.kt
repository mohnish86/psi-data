package com.psidata.data.repository

import com.psidata.BuildConfig
import com.psidata.data.models.MoviesData
import com.psidata.data.models.PsiModel
import com.psidata.data.models.bindingmodels.MoviesBindingModel
import com.psidata.data.services.MovieDataService
import com.psidata.data.services.PsiDataService
import com.psidata.data.services.networking.Resource
import com.psidata.data.services.networking.ResponseHandler
import org.koin.dsl.module

val movieDataRepo = module {
    single { MovieDataRepository(get(), get()) }
}

class MovieDataRepository(private val movieDataApi: MovieDataService, private val responseHandler: ResponseHandler) {
    suspend fun getMovieDataFromAPI(): Resource<MoviesData> {
        return try {
            responseHandler.handleSuccess(movieDataApi.getMovieData(BuildConfig.MOVIES_API_KEY))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}