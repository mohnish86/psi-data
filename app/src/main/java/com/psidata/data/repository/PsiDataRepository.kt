package com.psidata.data.repository

import com.psidata.data.models.PsiModel
import com.psidata.data.services.PsiDataService
import com.psidata.data.services.networking.Resource
import com.psidata.data.services.networking.ResponseHandler
import org.koin.dsl.module

val psiDataModuleRepo = module {
    factory { PsiDataRepository(get(), get()) }
}

class PsiDataRepository(private val psiDataApi: PsiDataService, private val responseHandler: ResponseHandler) {
    suspend fun getPsiDataFromAPI(): Resource<PsiModel> {
        return try {
            responseHandler.handleSuccess(psiDataApi.getPsiData())
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}