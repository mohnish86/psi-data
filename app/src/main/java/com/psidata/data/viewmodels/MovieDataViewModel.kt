package com.psidata.data.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.psidata.data.models.MoviesData
import com.psidata.data.models.bindingmodels.MoviesBindingModel
import com.psidata.data.repository.MovieDataRepository
import com.psidata.data.services.networking.Resource
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val movieModelModule = module {
    single { MovieDataViewModel(get()) }
}

class MovieDataViewModel(private val movieDataRepo: MovieDataRepository) : ViewModel() {

    val movieData: LiveData<Resource<MoviesData>> = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(movieDataRepo.getMovieDataFromAPI())
    }
}