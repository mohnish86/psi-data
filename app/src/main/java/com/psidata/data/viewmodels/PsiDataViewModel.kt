package com.psidata.data.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.psidata.data.models.PsiModel
import com.psidata.data.repository.PsiDataRepository
import com.psidata.data.services.networking.Resource
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val dataModelModule = module {
    factory { PsiDataViewModel(get()) }
}

class PsiDataViewModel(private val psiDataRepo: PsiDataRepository) : ViewModel() {

    val psiData: LiveData<Resource<PsiModel>> = liveData(Dispatchers.IO) {
        emit(Resource.loading())
        emit(psiDataRepo.getPsiDataFromAPI())
    }

    val psiDataMock: LiveData<Resource<PsiModel>> = liveData(Dispatchers.IO) {
//        emit(Resource.loading())
//        emit(psiDataRepo.get())
    }
}