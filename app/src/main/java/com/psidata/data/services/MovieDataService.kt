package com.psidata.data.services

import com.psidata.data.models.MoviesData
import com.psidata.data.models.bindingmodels.MoviesBindingModel
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieDataService {

    @Headers("Content-Type: application/json")
    @GET("/movies")
    suspend fun getMovieData(@Query("api_key") api_key: String): MoviesData

}