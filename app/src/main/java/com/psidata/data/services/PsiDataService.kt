package com.psidata.data.services

import com.psidata.data.models.PsiModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface PsiDataService {

    @Headers("Content-Type: application/json")
    @GET("/v1/environment/psi/")
    suspend fun getPsiData(): PsiModel


    @Headers("Content-Type: application/json")
    @GET("/v1/environment/psi/")
    fun getPsiBoxData(): Call<PsiModel>

}