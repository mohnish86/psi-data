package com.psidata.data.services.networking

import com.psidata.BuildConfig
import com.psidata.data.services.MovieDataService
import com.psidata.data.services.PsiDataService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { AuthInterceptor() }
    single { provideOkHttpClient(get(), get()) }
    single { providePsiDataApi(get()) }
    single { provideRetrofit(get()) }
    single { provideOkHttpLoggingInterceptor() }
    single { provideMovieDataApi(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.MOVIES_API_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(authInterceptor: AuthInterceptor, okhttpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(authInterceptor).addInterceptor(okhttpLoggingInterceptor).build()
}

fun provideOkHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    return logging
}

fun providePsiDataApi(retrofit: Retrofit): PsiDataService = retrofit.create(PsiDataService::class.java)

fun provideMovieDataApi(retrofit: Retrofit): MovieDataService = retrofit.create(MovieDataService::class.java)
