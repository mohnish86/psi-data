package com.psidata.application

import android.app.Application
import com.psidata.box.PsiBoxDataRepository
import com.psidata.box.psiBoxDataRepository
import com.psidata.data.repository.movieDataRepo
import com.psidata.data.services.networking.networkModule
import com.psidata.data.services.networking.psiDataHandler
import com.psidata.data.viewmodels.dataModelModule
import com.psidata.data.repository.psiDataModuleRepo
import com.psidata.data.viewmodels.movieModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(
                listOf(
                    movieDataRepo,
                    movieModelModule,
                    networkModule,
                    dataModelModule,
                    psiDataModuleRepo,
                    psiDataHandler,
                    psiBoxDataRepository
                )
            )
        }
    }
}