package com.psidata.helper

import com.google.gson.Gson

fun <T> String.fromGson(clazz: Class<T>): T = Gson().fromJson(this, clazz)