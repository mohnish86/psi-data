package com.psidata.helper

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

/**
 * This class is used as app overall extension functions
 */

@BindingAdapter("imageUrl")
fun ImageView.setImage(url: String) {
    Glide
        .with(context)
        .load(url)
        .centerCrop()
        .into(this)
}