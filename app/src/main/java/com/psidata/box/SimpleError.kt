package com.psidata.box

data class SimpleError(val errorCode: String?, val errorMessage: String?)