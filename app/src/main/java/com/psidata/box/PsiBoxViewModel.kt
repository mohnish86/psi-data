package com.psidata.box

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import org.koin.core.KoinComponent
import org.koin.core.get


abstract class PsiBoxViewModel(application: Application) : AndroidViewModel(application), KoinComponent{

    val psiBoxDataRepository: PsiBoxDataRepository = get()
}