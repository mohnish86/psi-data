package com.psidata.box

import com.psidata.data.models.BaseModel
import com.psidata.data.models.PsiModel
import com.psidata.data.services.PsiDataService
import com.psidata.helper.fromGson
import org.koin.core.KoinComponent
import org.koin.dsl.module
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

/**
 * Class is used for API calls &
 * declared as KoinComponent for injection purpose.
 */

val psiBoxDataRepository = module {
    single { PsiBoxDataRepository(get()) }
}

class PsiBoxDataRepository(private val psiDataService: PsiDataService) : KoinComponent {

    val path: String = "src/test/res/"

    private fun getPsiBoxData(): Call<PsiModel> {
        return psiDataService.getPsiBoxData()
    }

    /**
     * @return response from API
     *
     */
    fun getPsiData(cbOnResult: (PsiModel?) -> Unit, cbOnError: (SimpleError?) -> Unit) {
        val call = getPsiBoxData()
        call.enqueue(object : Callback<PsiModel> {
            override fun onResponse(call: Call<PsiModel>, response: Response<PsiModel>) {
                if (response.isSuccessful) {
                    cbOnResult(response.body()) // success
                } else {
                    cbOnError(SimpleError("", ""))
                }
            }

            override fun onFailure(call: Call<PsiModel>, t: Throwable) {
                // - there is more than just a failing request (like: no internet connection)
                // - can pass any error message or can handle as per the specific error code
                cbOnError(SimpleError("", ""))
            }
        })
    }

    /**
     * @return mock data for testing
     *
     */
    fun getPsiMockData(cbOnResult: (PsiModel?) -> Unit, cbOnError: (SimpleError?) -> Unit) {
        cbOnResult(getMockData() as PsiModel) // success
        cbOnError(SimpleError("", ""))
    }

    fun getMockData(): BaseModel {
        val file = File(path + "mockPsiDataSuccess.json")
        val jsonDataFile: String = file.readText()
        return jsonDataFile.fromGson(PsiModel::class.java)
    }
}